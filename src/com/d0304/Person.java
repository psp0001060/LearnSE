package com.d0304;

public class Person {
    //成员变量
    int age;
    String  name;

    public void changeName(String pname){
        //局部变量
        int a ;
        //System.out.println(a); //错误，原因是局部变量需要显示初始化
        name = pname;
    }

    public void showName(){
        System.out.println(name);
    }

    public void changeAge(int page){
        age = page;
    }

    //定义一个方法showAge,打印年龄
    public void showAge(){
        System.out.println(age);
    }

    public void  changeAndShowAge(int page){
        changeAge(page);
        showAge();
    }

    public void show(){
        String companyName = "Chinasofti";
        System.out.println(companyName);
    }

    public static void main(String[] args) {
        Person person = new Person();
        person.changeName("Lucy");
        person.showName();
    }

}
