package com.d0323;

public class Course2 {
    private String  title;
    private double price;

    public Course2(String title, double price) {
        this.title = title;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Course2 course2 = (Course2) o;

        if (Double.compare(course2.price, price) != 0) return false;
        return title != null ? title.equals(course2.title) : course2.title == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = title != null ? title.hashCode() : 0;
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public static void main(String[] args) {
        Course2 c1 = new Course2("math", 100);
        Course2 c2 = new Course2("math", 100);
        System.out.println(c1==c2);//比较的是两个对象的虚地址，false
        System.out.println(c1.equals(c2)); //比较的是两个对象的内容
    }
}
