package com.d0323;

import java.io.*;

public class ByteArrayBufferTest {
    static byte[] myBuffer;

    public static void main(String[] args) {
        writeBuffer();
        readBuffer();
    }

    //        将数据写入内存
    private static void writeBuffer() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        try {
            dos.writeInt(100);
            dos.writeDouble(3.14);
            dos.writeUTF("icss");
            myBuffer = baos.toByteArray();
            dos.close();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //        将内存数据读入，并打印
    private static void readBuffer() {
        ByteArrayInputStream bais = new ByteArrayInputStream(myBuffer);
        DataInputStream dis = new DataInputStream(bais);
        try {
            System.out.println(dis.readInt() + "\t" + dis.readDouble() + "\t" + dis.readUTF());
            dis.close();
            bais.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
