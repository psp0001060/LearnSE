package com.d0323;

import java.io.*;

public class SerializableTest {
    public static void main(String[] args) throws Exception {
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        FileOutputStream fos  = new FileOutputStream(new File("src/com/d0323/seriData.txt"));
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        Person person = new Person("Jack", 22, "Dalian");
        oos.writeObject(person);
        oos.close();
        fos.close();

        FileInputStream fis = new FileInputStream(new File("src/com/d0323/seriData.txt"));
        ObjectInputStream ois = new ObjectInputStream(fis);
        Person person1 = (Person) ois.readObject();
        System.out.println(person1);
        ois.close();
        fis.close();

    }
}
