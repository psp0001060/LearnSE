package com.d0323;

public class BubbleSort {
    public static void main(String[] args) {
        int[] numbers = {5, 1, 4, 2, 8, 9};
        int i, j;

        for (i = 0; i < numbers.length - 1; i++) { //趟数
            for (j = 0; j < numbers.length - 1 - i; j++) {
                if (numbers[j] > numbers[j+1]){
                    int temp = numbers[j];
                    numbers[j]= numbers[j+1];
                    numbers[j+1]=temp;
                }
            }
        }
        System.out.println("有小到大排序：");
        for (int k = 0; k < numbers.length; k++) {
            System.out.print(numbers[k]+" ");
        }
    }
}
