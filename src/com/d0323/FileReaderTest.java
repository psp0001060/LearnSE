package com.d0323;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class FileReaderTest {
    public static void main(String[] args) throws Exception {
        FileReader fr = new FileReader(new File("src/com/d0323/writerData.txt"));
        char[] buf = new char[1024];
        int c = 0;
        while ((c = fr.read(buf, 0, buf.length)) != -1) {
            System.out.println(new String(buf, 0, c));
        }
        fr.close();
    }

}
