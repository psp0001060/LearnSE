package com.d0323;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class FileReaderWithBufferTest {
    public static void main(String[] args) throws Exception {
        FileReader fr = new FileReader(new File("src/com/d0323/writerData.txt"));
        BufferedReader br = new BufferedReader(fr);
        String str = "";
        while ((str= br.readLine()) != null) {
            System.out.println(str);
        }
        br.close();
        fr.close();
    }

}
