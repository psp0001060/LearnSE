package com.d0323;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterTest {
    public static void main(String[] args) throws IOException {
        FileWriter fw = new FileWriter(new File("src/com/d0323/writerData.txt"));
        BufferedWriter bw = new BufferedWriter(fw); //采用缓冲流，效率更高
        bw.write("hello chinasofti");
        bw.close();
        fw.close();
    }
}
