package com.d0323;

public class Sheep implements Cloneable{
    private String  name;
    private int age;

    public Sheep(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Sheep s = (Sheep) super.clone();
        s.name = new String(s.name);
        return s;
    }

    public static void main(String[] args) {
        Sheep s1 = new Sheep("Alice", 3);
        try {
            Sheep duoli = (Sheep) s1.clone();
            System.out.println(s1.name==duoli.name);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
