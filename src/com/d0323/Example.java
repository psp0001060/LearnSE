package com.d0323;

import java.io.IOException;
import java.io.RandomAccessFile;

public class Example {
    public static void main(String[] args) throws IOException {
        RandomAccessFile file = new RandomAccessFile("src/com/d0323/test.txt", "rw");
        file.writeBoolean(true);
        file.writeInt(123456);
        file.writeInt(7890);
        file.writeLong(10000000);
        file.writeInt(777);
        file.writeFloat(0.0001f);
        file.seek(17);
        System.out.println(file.readInt());
        file.close();
    }
}
