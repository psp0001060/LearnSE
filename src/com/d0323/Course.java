package com.d0323;

public class Course {
    private String  title;
    private double price;

    public Course(String title, double price) {
        this.title = title;
        this.price = price;
    }

    public static void main(String[] args) {
        Course c1 = new Course("math", 100);
        Course c2 = new Course("math", 100);
        System.out.println(c1==c2);//比较的是两个对象的虚地址，false
        System.out.println(c1.equals(c2)); //比较的是两个对象的虚地址，false
    }
}
