package com.d0312;

public class AnonymousInnerClass {
    private String name = "Jerry";
    private int age = 24;
//    IFoo foo = new IFoo() {
//        @Override
//        public void display() {
//            System.out.println("hello");
//        }
//    };

    //    IFoo foo = () -> System.out.println("hello");
//    IFoo foo = () -> 5;
//    IFoo foo = (x,y) -> x+y;
//    IFoo foo = (x,y) -> x+y;
    IFoo foo = (String s) -> System.out.println(s + "\t" +name);


    public static void main(String[] args) {
        AnonymousInnerClass al = new AnonymousInnerClass();
        al.foo.showStr("world");
    }

}

@FunctionalInterface
interface IFoo {
    //    public void display();
//    int getResult();
//     int get2Result(int a);
//    int sum(int a, int b);
    default void foo(){

    }
    void showStr(String str);
}

abstract class House {
    public abstract void show();
}