package com.d0312;

public class OuterClass3 {
    int i =10;

    class InnerClass{
        int i =100;
        public void display(){
            System.out.println(this.i);
            System.out.println(OuterClass3.this.i);
        }
    }

    public static void main(String[] args) {
        OuterClass3 outerClass3 = new OuterClass3();
        OuterClass3.InnerClass innerClass = outerClass3.new InnerClass();
        innerClass.display();
    }
}
