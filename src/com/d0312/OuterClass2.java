package com.d0312;

public class OuterClass2 {
    private String sex;
    public static String name = "Jerry";

    static class InnerClass1{
        public static String staticName = "Tom";

        public void display(){
            System.out.println("staticName="+staticName);
            System.out.println("name="+name);
//            System.out.println("sex="+sex); 错误，静态内部类不能访问外部的非静态成员
        }
    }
    public void show(){
        System.out.println(InnerClass1.staticName);
        InnerClass1 innerClass1 = new OuterClass2.InnerClass1();
        innerClass1.display();
    }

    public static void main(String[] args) {
        OuterClass2 outerClass2 = new OuterClass2();
        outerClass2.show();
    }
}
