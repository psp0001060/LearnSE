package com.d0312;

public class OuterClass {
    private String name;
    private int age;
    public OuterClass(String name, int age) {
        this.name = name;
        this.age = age;
    }
    class InnerClass {
//        static int a; //内部类当中不能出现static的变量和方法
//        public InnerClass(){
//            name="john";
//        }
        public void display(){
            System.out.println(name + "\t" +age);
        }
    }

    public static void main(String[] args) {
        OuterClass outerClass = new OuterClass("JACK",22);
        InnerClass innerClass = outerClass.new InnerClass();
        innerClass.display();
    }
}
