package com.d0312;

public class OuterClass1 {
    private String name;
    private int age;

    public void show(final int a){ //方法的参数a，要求是final常量
          class InnerClass {
            public InnerClass(){
                name="john";
                age = a;
            }
            public void display(){
                System.out.println(name + "\t" +age);
            }
        }

        InnerClass innerClass = new InnerClass();
        innerClass.display();
    }

    public static void main(String[] args) {
        OuterClass1 outerClass1 = new OuterClass1();
        outerClass1.show(23);
    }
}
