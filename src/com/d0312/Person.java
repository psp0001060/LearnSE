package com.d0312;

public class Person {
    private int age;
    private String name;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class TestPerson{
    public static void main(String[] args) {
        Person person = new Person();
        person.setName("jack");
        System.out.println(person.getName());
    }
}
