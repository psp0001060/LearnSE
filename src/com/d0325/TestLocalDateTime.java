package com.d0325;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class TestLocalDateTime {
    public static void main(String[] args) {
//        LocalDateTime ldt = LocalDateTime.now();
//        LocalDateTime ldt2 = LocalDateTime.of(2020, 12, 8, 12, 23, 23);
//        System.out.println(ldt);
//        System.out.println(ldt2);
//
//        String str ="2023-12-08T12:23:23";
//        LocalDateTime ldt3 = LocalDateTime.parse(str);
//        System.out.println(ldt3);

        String patt = "HH时mm分ss秒";
        String str = "10时15分30秒";
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(patt);
        LocalTime localTime = LocalTime.parse(str, dtf);
        System.out.println(localTime);

    }
}
