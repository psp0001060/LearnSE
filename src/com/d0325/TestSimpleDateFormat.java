package com.d0325;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TestSimpleDateFormat {
    public static void main(String[] args) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy年MM月dd日");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");

        Date date = new Date();
        System.out.println(sdf1.format(date));
        System.out.println(sdf2.format(date));

        Calendar calendar = Calendar.getInstance();
        calendar.set(2020,Calendar.MARCH,31,10,23,45);
        System.out.println(sdf1.format(calendar.getTime()));
        System.out.println(sdf2.format(calendar.getTime()));


        //String ==> Date
        String birth = "1999年03月28日";
        try {
            Date birDate = sdf1.parse(birth);
            System.out.println(birDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}
