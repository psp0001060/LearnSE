package com.d0325;

public class GenericMethodTest {
    public static <E> void printArray(E[] arrs) {
        for (E i :
                arrs) {
            System.out.println(i);
        }
    }


    public static void main(String[] args) {
        Integer[] intArrs = {1, 2, 3, 4, 5};
        String[] strArrs = {"ab", "cd", "ef"};
        Double[] douArrs = {1.1, 1.2, 1.3, 1.4};

        printArray(intArrs);
        printArray(strArrs);
        printArray(douArrs);

    }
}
