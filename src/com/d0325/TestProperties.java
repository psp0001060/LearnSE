package com.d0325;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class TestProperties {
    public static void main(String[] args) {
        try {
            FileReader fr = new FileReader(new File("src/com/d0325/message.properties"));
            Properties properties = new Properties();
            properties.load(fr);
            System.out.println(properties.getProperty("username"));
            System.out.println(properties.getProperty("pwd"));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
