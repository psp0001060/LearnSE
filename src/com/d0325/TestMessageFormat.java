package com.d0325;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.MessageFormat;
import java.util.Properties;

public class TestMessageFormat {
    public static void main(String[] args) throws Exception {
        FileReader fr = new FileReader(new File("src/com/d0325/message.properties"));
        Properties properties = new Properties();
        properties.load(fr);
        String pattern = properties.getProperty("pattern");
        System.out.println(MessageFormat.format(pattern,new Object[]{"Alice",123}));
        System.out.println(MessageFormat.format(pattern,new Object[]{"Lily",135}));
    }
}
