package com.d0325;

public class BoxTest<T> {
    private T t;

    public void add(T t){
        this.t =t;
    }

    public T get(){
        return t;
    }

    public static void main(String[] args) {
        BoxTest<Integer> integerBox = new BoxTest<Integer>();
        BoxTest<String> strBox = new BoxTest<>();

        integerBox.add(new Integer(10));
        strBox.add(new String("world"));

        System.out.println(integerBox.get());
        System.out.println(strBox.get());

    }
}
