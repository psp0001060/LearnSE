package com.d0325;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class TestArrayList {
    public static void main(String[] args) {
        List<Integer> myList = new ArrayList<Integer>();
         myList.add(0,11);
         myList.add(1,12);
         myList.add(2,13);
//         myList.add(1,23);
        myList.add(44);
//        System.out.println(myList.isEmpty());
//        System.out.println(myList.contains(2));
//        myList.remove(3);
        System.out.println("元素的个数："+myList.size());
//        System.out.println(myList.get(1));
        List<Integer> s2 = myList.subList(1,3);

        for (Integer i:
             s2) {
            System.out.println(i);
        }
        System.out.println("================");
        Iterator<Integer> iterator = s2.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

    }
}
