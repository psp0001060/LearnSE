package com.d0325;

public class SimpleGen<T> {
    private T ob;

    public SimpleGen(T ob) {
        this.ob = ob;
    }

    public T getOb() {
        return ob;
    }

    public void setOb(T ob) {
        this.ob = ob;
    }

    public static void main(String[] args) {
        SimpleGen<Integer> sg = new SimpleGen<>(new Integer(99));
        Integer i = sg.getOb();
        System.out.println(i);

        SimpleGen<String> sg2 = new SimpleGen<>(new String("hello"));
        String str = sg2.getOb();
        System.out.println(str);
    }
}


