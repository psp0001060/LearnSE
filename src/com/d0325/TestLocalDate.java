package com.d0325;

import java.time.LocalDate;

public class TestLocalDate {
    public static void main(String[] args) {
        LocalDate date1 = LocalDate.now();
        System.out.println(date1);

        LocalDate date2 = LocalDate.of(2021, 5, 21);
        System.out.println(date2);

        System.out.println(date2.getYear());
        System.out.println(date2.getMonth());

        String str = "2022-09-12";
        LocalDate date3 = LocalDate.parse(str);
        System.out.println(date3);
    }
}
