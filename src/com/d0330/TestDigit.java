package com.d0330;

import java.util.regex.Pattern;

public class TestDigit {
    public static boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }

    public static boolean isDouble(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?[.\\d]*$");
        return pattern.matcher(str).matches();
    }

    public static void main(String[] args) {
//        System.out.println(isNumeric("45"));
//        System.out.println(isNumeric("-45"));
//        System.out.println(isNumeric("a45"));
        System.out.println(isDouble("4.5"));


    }
}
