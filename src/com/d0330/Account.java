package com.d0330;

public class Account {
    private  String accountId;
    private String pwd;
    private double balance;

    public Account() {
    }

    public Account(String accountId, String pwd, double balance) {
        this.accountId = accountId;
        this.pwd = pwd;
        this.balance = balance;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Double save(Double amount){
        balance =balance+amount;
        return balance;
    }

    public Double pay(Double amount){
        balance =balance-amount;
        return balance;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountId='" + accountId + '\'' +
                ", pwd='" + pwd + '\'' +
                ", balance=" + balance +
                '}';
    }
}
