package com.d0330;

public class TestNewInstance {
    public static void main(String[] args) {
        Class clazz = Account.class;

        try {
            //使用字节码来实例化对象
            Account account = (Account) clazz.newInstance();
            System.out.println(account);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }
}
