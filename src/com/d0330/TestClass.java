package com.d0330;

public class TestClass {
    public static void main(String[] args) {
        //1. 使用类名.class 方式可以获得字节码
        Class clazz1 = Account.class;

        //2.使用Class.forName方法取得字节码
        try {
            Class clazz2 = Class.forName("com.d0330.Account");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        //3. 使用对象取得
        Account account = new Account();
        Class clazz3 = account.getClass();
    }
}
