package com.d0330;

import java.io.*;
import java.net.Socket;

public class TCPClient {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("127.0.0.1",4700);
            BufferedReader sin = new BufferedReader(new InputStreamReader(System.in));//键盘输入内容
            String line = sin.readLine();

            DataOutputStream os = new DataOutputStream(socket.getOutputStream());
            DataInputStream is = new DataInputStream(socket.getInputStream());

            while (!line.equals("exit")) {
                //向服务器写数据
                os.writeUTF(line);
                os.flush();
                System.out.println("Server:" + is.readUTF());
                line = sin.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
