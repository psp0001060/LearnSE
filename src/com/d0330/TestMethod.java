package com.d0330;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

public class TestMethod {
    public static void main(String[] args) {
        Class clazz = Account.class;

        try {
            FileReader fr = new FileReader(new File("src/com/d0330/config.properties"));
            Properties properties =new Properties();
            properties.load(fr);
            //获得Method对象，方法名是save,参数是Double
            Method method = clazz.getMethod(properties.getProperty("todoMethod"), Double.class);
/*            System.out.println(method.getName());
            for (Class clz :
                    method.getParameterTypes()) {
                System.out.println(clz);
            }*/

            //创建对象
            Account account = (Account) clazz.newInstance();

            System.out.println(account.getBalance());

            //调用save
            method.invoke(account, new Double(2000));

            System.out.println(account.getBalance());

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
