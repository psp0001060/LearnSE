package com.d0330;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class TestConstructor {
    public static void main(String[] args) {
        //通过类的字节码来获得构造函数
        try {
//            Constructor<Account> con = Account.class.getConstructor();
//            Account account = con.newInstance();
//
            Constructor<Account> con = Account.class.getConstructor(String.class, String.class, double.class);
            Account account = con.newInstance("188", "123", 8000);
            System.out.println(account);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
