package com.d0330;

public class Employee {
    private  String accountId;
    private String pwd;
    private double salary;

    public Employee() {
    }

    public Employee(String accountId, String pwd, double salary) {
        this.accountId = accountId;
        this.pwd = pwd;
        this.salary = salary;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "accountId='" + accountId + '\'' +
                ", pwd='" + pwd + '\'' +
                ", salary=" + salary +
                '}';
    }
}
