package com.d0330;

import java.io.*;
import java.util.Properties;

public class TestRefectValue {
    public static void main(String[] args) {
        //实例化一个对象
        try {
            FileReader fr = new FileReader(new File("src/com/d0330/config.properties"));
            Properties properties =new Properties();
            properties.load(fr);
            Class clazz1 = Class.forName(properties.getProperty("todoClass"));
            Object obj = clazz1.newInstance();
            if (obj instanceof Account){
                System.out.println((Account)obj);
            }else if (obj instanceof Employee){
                System.out.println((Employee)obj);
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
