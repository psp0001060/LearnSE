package com.d0318;

import java.util.concurrent.TimeUnit;

public class Resource2 {
    private  Object syncObject1 = new Object();
    private  String syncObject2 = new String();
    public void f() {
        System.out.println(Thread.currentThread().getName() + " not synchronized f() is running.");
        synchronized (this) {
            for (int i = 0; i < 5; i++) {
                System.out.println(Thread.currentThread().getName() + "synchronized f() is running.");
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } 
        }
    }

    public void g() {
        System.out.println(Thread.currentThread().getName() + " not synchronized g() is running.");
        synchronized (syncObject1) {
            for (int i = 0; i < 5; i++) {
                System.out.println(Thread.currentThread().getName() + "synchronized g() is running.");
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void h() {
        System.out.println(Thread.currentThread().getName() + " not synchronized h() is running.");
        synchronized (syncObject2) {
            for (int i = 0; i < 5; i++) {
                System.out.println(Thread.currentThread().getName() + "synchronized h() is running.");
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static void main(String[] args) {
        Resource2 resource = new Resource2();
        new Thread(() -> resource.f()).start();
        new Thread(() -> resource.g()).start();
        resource.h();
    }
}

/*

class  MyThread extends  Thread{
    Resource resource = new Resource();
    @Override
    public void run() {
        resource.f();
    }
}*/
