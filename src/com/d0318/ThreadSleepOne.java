package com.d0318;

public class ThreadSleepOne implements Runnable {
    private String name;

    @Override
    public void run() {
        for (int i = 0; i <10; i++) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name +" ： "+i);
        }
    }

    public static void main(String[] args) {
        ThreadSleepOne one = new ThreadSleepOne();
        ThreadSleepOne two = new ThreadSleepOne();
        one.name = "one";
        two.name = "two";

        Thread t1 = new Thread(one);
        Thread t2 = new Thread(two);
        t1.setPriority(Thread.MAX_PRIORITY);
        t2.setPriority(Thread.MIN_PRIORITY);

        System.out.println(t1);
        System.out.println(t2);

        t1.start();
        t2.start();
    }
}
