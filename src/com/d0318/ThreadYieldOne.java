package com.d0318;

public class ThreadYieldOne implements Runnable {
    private String name;

    @Override
    public void run() {
        for (int i = 0; i <10; i++) {
            Thread.yield();//使进程进入到就绪状态
            System.out.println(name +" ： "+i);
        }
    }

    public static void main(String[] args) {
        ThreadYieldOne one = new ThreadYieldOne();
        ThreadYieldOne two = new ThreadYieldOne();
        one.name = "one";
        two.name = "two";

        Thread t1 = new Thread(one);
        Thread t2 = new Thread(two);
        t1.setPriority(Thread.MAX_PRIORITY);
        t2.setPriority(Thread.MIN_PRIORITY);

        System.out.println(t1);
        System.out.println(t2);

        t1.start();
        t2.start();
    }
}
