package com.d0318;

import java.util.Date;

public class JoinTester implements Runnable {
    private String name;

    public JoinTester(String name){
        this.name =name;
    }


    @Override
    public void run() {
        System.out.println(name +" 线程开始于："+ new Date());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(name +" 线程结束于："+ new Date());
    }

    public static void main(String[] args) {
        Thread thread1 = new Thread(new JoinTester("One"));
        Thread thread2 = new Thread(new JoinTester("Two"));
        thread2.start();
        thread1.start();

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("主线程结束");
    }
}
