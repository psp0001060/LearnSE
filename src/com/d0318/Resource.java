package com.d0318;

import java.util.concurrent.TimeUnit;

public class Resource {
    public synchronized void f(){
        for (int i = 0; i <5 ; i++) {
            System.out.println(Thread.currentThread().getName() +" f() is running.");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void g(){
        for (int i = 0; i <5 ; i++) {
            System.out.println(Thread.currentThread().getName() +" g() is running.");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void h(){
        for (int i = 0; i <5 ; i++) {
            System.out.println(Thread.currentThread().getName() +" h() is running.");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Resource resource = new Resource();
        new Thread(()->resource.f()).start();
        new Thread(()->resource.g()).start();
        resource.h();
    }
}

/*

class  MyThread extends  Thread{
    Resource resource = new Resource();
    @Override
    public void run() {
        resource.f();
    }
}*/
