package com.d0324;

public class Sheep2 implements Cloneable{
    private String  name;
    private int age;
    private Person owner;

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public Sheep2(String name, int age, Person owner) {
        this.name = name;
        this.age = age;
        this.owner = owner;
    }

    public Sheep2(String name, int age) {
        this.name = name;
        this.age = age;
    }

//    @Override
//    protected Object clone() throws CloneNotSupportedException {
//        Sheep2 sheep2 = (Sheep2) super.clone();
//        Person o2 = new Person(sheep2.getOwner().getName(),sheep2.getOwner().getAge(),sheep2.getOwner().getAddress());
//        Sheep2 oth = new Sheep2(this.name, this.age, o2);
//        return oth;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static void main(String[] args) {
        Person o1 = new Person("Own1",22,"Dalian");
        Sheep2 s1 = new Sheep2("Alice", 3,o1);
        try {
            Sheep2 duoli = (Sheep2) s1.clone();
            System.out.println(s1.getOwner().getAge());
            System.out.println(duoli.getOwner().getAge());
            System.out.println("======================");
//            o1.setAge(33);
            s1.setName("fl");
//            System.out.println(s1.getOwner().getAge());
//            System.out.println(duoli.getOwner().getAge());
            System.out.println(s1.getName());
            System.out.println(duoli.getName());
//            System.out.println("duoli.getOwner().getAge():"+duoli.getOwner().getAge());
      } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
