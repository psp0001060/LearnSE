package com.d0324;

import java.util.Calendar;
import java.util.Date;

public class DateTest {
    public static void main(String[] args) {
//        Date date = new Date(10*24*60*60*1000);
//        System.out.println(date);

        //距离1970.1.1  8点 至此刻的时间差的毫秒
//        System.out.println(System.currentTimeMillis());

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR,2035);
        System.out.println(calendar.get(Calendar.MONTH)+1); //月份是从0开始
        calendar.add(Calendar.YEAR,2);
        System.out.println(calendar.getTime());

    }
}
