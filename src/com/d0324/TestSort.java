package com.d0324;

import java.util.Arrays;

public class TestSort {
    public static void main(String[] args) {
        Product p1 = new Product("巧克力", 45.5, 1);
        Product p2 = new Product("教材", 30, 5);
        Product p3 = new Product("衣服", 145.5, 10);
        Product p4 = new Product("手机", 3445.5, 2);
        Product[] products = new Product[]{p1, p2, p3, p4};

        Arrays.sort(products);

        for (Product prod:
             products) {
            System.out.println(prod);
        }

    }
}
