package com.d0324;

import java.util.Arrays;

public class TestStudent {
    public static void main(String[] args) {
        Student s1 = new Student("肖战",23,89);
        Student s2 = new Student("王一博",22,87);
        Student s3 = new Student("邓紫棋",24,95);
        Student s4 = new Student("曲特",21,94);

        Student[] students = new Student[]{s1,s2,s3,s4};
        Arrays.sort(students,new StudentAgeComparator());

        for (Student stu:
             students) {
            System.out.println(stu);
        }

    }
}
