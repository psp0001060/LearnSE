package com.d0324;

public class Product implements Comparable<Product> {//泛型
    private String title;
    private double price;
    private int saleVolume;

    public Product(String title, double price, int saleVolume) {
        this.title = title;
        this.price = price;
        this.saleVolume = saleVolume;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getSaleVolume() {
        return saleVolume;
    }

    public void setSaleVolume(int saleVolume) {
        this.saleVolume = saleVolume;
    }

    @Override
    public String toString() {
        return title + "\t" + price + "\t" + saleVolume;
    }

    @Override
    public int compareTo(Product o) {
        if (this.price > o.price) {
            return 1;
        } else if (this.price < o.price) {
            return -1;
        } else {
            return 0;

        }
    }
}
