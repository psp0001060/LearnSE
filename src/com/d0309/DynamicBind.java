package com.d0309;

public class DynamicBind {
    public void foo(){
        System.out.println("父类中的方法");
    }

    public void testArg(DynamicBind arg){
        arg.foo();
    }



    //编写代码验证成员方法是否是动态绑定
    public static void main(String[] args) {
        DynamicBind dynamicBind = new DynamicBindSub();
//        dynamicBind.foo();
//        dynamicBind.testArg(dynamicBind);
    }
}

class DynamicBindSub extends DynamicBind{
    public void foo(){
        System.out.println("子类中的方法");
    }

}