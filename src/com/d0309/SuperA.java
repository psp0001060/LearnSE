package com.d0309;

public class SuperA {
    public float getNum() {
        return 3.0f;
    }
}

class Sub extends SuperA {
    public void getNum(double d){}
}