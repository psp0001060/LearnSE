package com.d0309;

public class Human {
    public void foo(Human human){
        if (human instanceof Men){
            System.out.println("纯爷们");
        }else {
            System.out.println("俏姑娘");
        }

    }

    public static void main(String[] args) {
        Men human = new Men();
        human.foo(human);
    }
}

class Men extends Human{

}

class Women extends Human{

}
