package com.d0309;

public class Bird {
    public int age = 10;
    protected void move(){
        System.out.println("飞翔");
    }

    public static void main(String[] args) {
        //向上转型
        Bird bird = new Penguin();
//        bird.show(); //错误的，编译器只能调用父类中的方法

        //向下造型
        ((Penguin) bird).show();

        Penguin  penguin = new Penguin();
        penguin.show();
    }
}
