package com.d0309;

public class StaticBind {
    public  int i = 10;

    public static void main(String[] args) {
        StaticBind staticBind = new StaticBindSub();
        System.out.println(staticBind.i);
    }
}

class  StaticBindSub extends  StaticBind{
    public int i =100;
}