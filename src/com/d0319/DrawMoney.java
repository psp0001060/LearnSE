package com.d0319;

public class DrawMoney {
    public static void main(String[] args) {
        Object o = new Object();
        Customer t1 =new Customer(o,"A");
        Customer t2 =new Customer(o,"B");
        t1.start();
        t2.start();
    }
}
//取钱的计算方法
class  Mbank{
    private  static  int sum = 1000;
    public static   int   take(String name,int k){
        if(sum > 0) {
            sum = sum - k;

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(name+"取走了"+k+"还剩"+sum+"元");
        }
       return sum;
    }
}

//实现多线程线程
class Customer extends  Thread{
    private String name;
    private Object lock;
    public  Customer(Object lock, String name){
        this.name=name;
        this.lock = lock;
    }
    @Override
    public void run() {
        while (true){
            synchronized (lock) {
                int temp = Mbank.take(name,100);
                if (temp <= 0) {
                    break;
                }
            }
        }
         /* for (int i = 0; i <5 ; i++) {
            Mbank.take(name,100);
          }*/
    }
}