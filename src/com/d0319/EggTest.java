package com.d0319;

public class EggTest {
    volatile boolean hasEggs = false;//提供同步的变量设置

    Thread human = new Thread(()->{
       while (true){
           if (!hasEggs){
               System.out.println("等待");
           }else {
               System.out.println("收取");
               hasEggs = false;
           }
       }
    });

    Thread hen = new Thread(()->{
        while (true){
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            hasEggs =true;
        }
    });

    public static void main(String[] args) {
        EggTest eggTest = new EggTest();
        eggTest.hen.start();
        eggTest.human.start();
    }
}
