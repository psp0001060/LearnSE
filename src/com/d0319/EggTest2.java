package com.d0319;

public class EggTest2 {
    volatile boolean hasEggs = false;//提供同步的变量设置
    Object flag = new Object();

    Thread human = new Thread(() -> {
        while (true) {
            if (!hasEggs) {
                synchronized (flag) {
                    try {
                        System.out.println( "human：等待");
                        flag.wait();//阻塞
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                System.out.println("human：收取");
                hasEggs = false;
            }
        }
    });


    Thread hen = new Thread(() -> {
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            hasEggs = true;
            System.out.println("hen: 已经产生鸡蛋");
            synchronized (flag) {
                flag.notify(); //唤醒
            }
        }
    });

    public static void main(String[] args) {
        EggTest2 eggTest = new EggTest2();
        eggTest.hen.start();
        eggTest.human.start();
    }
}
