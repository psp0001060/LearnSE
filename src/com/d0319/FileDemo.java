package com.d0319;

import java.io.File;
import java.io.IOException;

public class FileDemo {
    public static void main(String[] args) {
        File file = new File("src/com/d0319/test.txt"); //相对路径
//        File file = new File("D:\\soft\\FeiQ.exe"); //绝对路径
//        System.out.println("文件是否存在："+file.exists());
        FileDemo fileDemo = new FileDemo();
//        fileDemo.deleteFile("src/com/d0319/test.txt");
        fileDemo.createFile2("src/com/d0319/dir1", "t2.txt");
    }

    void deleteFile(String path) {
        File f = new File(path);
        System.out.println(f.delete() == true ? "删除成功" : "删除失败");
    }

    void createFile(String path, String fileName) {
        File f = new File(path + File.separator + fileName);
        try {
            System.out.println(f.createNewFile()==true?"创建成功":"创建失败");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*
    在src/com/d0319下新建一个目录，名称dir1,
    在dir1中新建一个空文件，名称是t2.txt
     */
    void createFile2(String path, String fileName) {
        File dir = new File(path);
        if (!dir.isDirectory()){
            dir.mkdir();
        }

        File f = new File(path + File.separator + fileName);
        try {
            System.out.println(f.createNewFile()==true?"创建成功":"创建失败");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
