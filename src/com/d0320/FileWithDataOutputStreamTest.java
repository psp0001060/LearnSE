package com.d0320;

import java.io.*;

public class FileWithDataOutputStreamTest {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        DataOutputStream dos = null;
        File file = new File("src/com/d0320/data.txt");
        try {
            fos = new FileOutputStream(file);
            dos = new DataOutputStream(fos);
            dos.writeInt(100);
            dos.writeDouble(3.1415926);
            dos.writeUTF("hello world");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != dos) {
                try {
                    dos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (null != fos) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
