package com.d0320;

import java.io.*;

public class FileCopy {
    public static void main(String[] args) {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            fis = new FileInputStream(new File("src/com/d0320/data.txt"));
            fos = new FileOutputStream(new File("src/com/d0320/data2.txt"));
            byte[] buffer = new byte[1024];
            int c = 0;
            while ((c = fis.read(buffer, 0, buffer.length)) != -1) {
                fos.write(buffer, 0, c);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != fis) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (null != fos) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
