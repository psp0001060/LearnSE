package com.d0320;

import java.io.*;

public class FileWithDataInputStreamTest {
    public static void main(String[] args) {
        FileInputStream fis = null;
        DataInputStream dis = null;

        File file = new File("src/com/d0320/data.txt");
        try {
            fis = new FileInputStream(file);
            dis = new DataInputStream(fis);
            System.out.println(dis.readInt() + "\t" + dis.readDouble() + "\t" + dis.readUTF());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != dis) {
                try {
                    dis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (null != fis) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
