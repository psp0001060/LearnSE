package com.d0326;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class TestHashSet {
    public static void main(String[] args) {
        Set<Student> set = new HashSet<>();

        Student stu1 = new Student("Jack", 21, 98);
        Student stu2 = new Student("Lucy", 22, 98);
        Student stu3 = new Student("Jack", 21, 98);

        set.add(stu1);
        set.add(stu2);
        set.add(stu3);
        System.out.println("元素的个数是：" + set.size());
/*
        for (Student stu:
             set) {
            System.out.println(stu);
        }
*/

/*        Iterator<Student> iterator = set.iterator();
        while (iterator.hasNext()){
            Student student = iterator.next();
            System.out.println(student);
        }*/

        set.forEach(student -> System.out.println(student));

    }
}
