package com.d0326;

import java.util.Collections;
import java.util.List;
import java.util.Vector;

public class TestCollections {
    public static void main(String[] args) {
        List<Integer> list = new Vector<>();
        list.add(22);
        list.add(11);
        list.add(99);

//        Collections.reverse(list);

//        Collections.sort(list);
/*        for (Integer i:
             list) {
            System.out.println(i);
        }*/

//        list.forEach(i -> System.out.println(i));

        int index = Collections.binarySearch(list, 99);
        System.out.println(index);
    }
}
