package com.d0326;

import java.util.Set;
import java.util.TreeSet;

public class TestTreeSet {
    public static void main(String[] args) {
        Set<Student> treeSet = new TreeSet(new StudentAgeComparator());
        Student s1 = new Student("肖战",23,89);
        Student s2 = new Student("王一博",22,87);
        Student s3 = new Student("邓紫棋",24,95);
        Student s4 = new Student("曲特",21,94);

        treeSet.add(s1);
        treeSet.add(s2);
        treeSet.add(s3);
        treeSet.add(s4);
        System.out.println("个数是："+treeSet.size());

  //按照年龄升排序
        for (Student stu:
             treeSet) {
            System.out.println(stu);
        }

    }
}
