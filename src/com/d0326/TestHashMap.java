package com.d0326;

import java.util.*;

import static javafx.scene.input.KeyCode.V;

public class TestHashMap {
    public static void main(String[] args) {
        Map<String, Student> studentMap = new HashMap<>();
        Student s1 = new Student("肖战", 23, 89);
        Student s2 = new Student("王一博", 22, 87);
        Student s3 = new Student("邓紫棋", 24, 95);
        Student s4 = new Student("曲特", 21, 94);

        studentMap.put("xz", s1);
        studentMap.put("wyb", s2);
        studentMap.put("dzq", s3);
        studentMap.put("qt", s4);
        studentMap.put("GEM", s3);

/*        System.out.println(studentMap.size());
        //key 不同，value 可以相同
        Student x1 = studentMap.get("dzq");
        System.out.println(x1);*/

/*        Set<String> keys = studentMap.keySet();
        for (String str:
             keys) {
            System.out.println(str +"------>"+ studentMap.get(str));
        }*/

        //key可以取得value，反过来不可以
/*        Collection<Student> values = studentMap.values();
        for (Student stu:
             values) {
            System.out.println(stu);
        }*/

/*        Set<Map.Entry<String,Student>> entys = studentMap.entrySet();
        for (Map.Entry entry:
             entys) {
            System.out.println(entry.getKey() + "--------->" + entry.getValue());
        }*/

        studentMap.forEach((K, V) -> {
            System.out.println(K + "-------->" + V);
        });
    }
}
