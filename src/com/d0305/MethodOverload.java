package com.d0305;

public class MethodOverload {

    int square(char x){
        System.out.println("square(char x)");
        return  x*x;
    }
    int square(short x){
        System.out.println("square(short x)");
        return  x*x;
    }

    long square(long x){
        System.out.println("square(long x)");
        return  x*x;
    }

    double square(double x){
        return  x*x;
    }

    public static void main(String[] args) {
        MethodOverload methodOverload = new MethodOverload();
        System.out.println(methodOverload.square(3));
//        System.out.println(methodOverload.square(4.5));
    }
}
