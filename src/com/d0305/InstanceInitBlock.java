package com.d0305;

public class InstanceInitBlock {
    int i,j;
    static int k;
    //实例化代码块
    {
        System.out.println("执行了实例化代码块");
        i=10;
        this.j=20;
        k=30;
    }

    public static void main(String[] args) {
        InstanceInitBlock instanceInitBlock = new InstanceInitBlock();
        InstanceInitBlock instanceInitBlock2 = new InstanceInitBlock();
        System.out.println(instanceInitBlock.i);
        System.out.println(instanceInitBlock.j);
        System.out.println(instanceInitBlock.k);
    }
}
