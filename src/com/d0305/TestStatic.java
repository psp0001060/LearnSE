package com.d0305;

public class TestStatic {
    int i =10;
    static int j =20;
    static {
//        i=30; //错误，static块不能操作非static变量
//        j=210; //可以
//        this.i=30; //错误，static块不能使用this/super
        System.out.println("当前类加载到了虚拟机里1");
    }

//    static {
//        System.out.println("当前类加载到了虚拟机里2");
//    }

    public TestStatic(){
        System.out.println("执行了初始化方法");
    }

    public static void main(String[] args) {
        TestStatic testStatic1 = new TestStatic();
        TestStatic testStatic2 = new TestStatic();
    }

}
