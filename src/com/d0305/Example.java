package com.d0305;

public class Example {

    public static int num = 0;
    String name;
    static {
        System.out.println(num + ":code A");
    }
    {
        System.out.println(num + ":code B");
        num++;
    }

    public Example(String name) {
        System.out.println(num + ":构造器：" + name);
        this.name = name;
        num++;
    }

    public static void main(String[] args) {
        Example people = new Example("Tom");
        Example people2 = new Example("Jim");
    }
}
