package com.d0305;

public class PhoneCard {
    long cardNumber;
    int passwd;
    double balance;

    public PhoneCard(){
        //使用this调用本类其他构造函数
        this(1396969696L,654321); //必须放到第一行
        System.out.println("调用另一个构造函数");
    }


    public PhoneCard(long p_cardNumber,int passwd){
        cardNumber=p_cardNumber;
        this.passwd = passwd;
    }


    public static void main(String[] args) {
        PhoneCard phoneCard = new PhoneCard();
//        PhoneCard phoneCard = new PhoneCard(13898900987L,123456);
        System.out.println(phoneCard.cardNumber + "\t"+phoneCard.passwd);
    }
}
