package com.d0305;

public class Test0305 {
    public static void main(String[] args) {
        int m = 2;
        int p = 1;
        int i = 0;
        for (; p < 5; p++) {
            System.out.println("p="+p);
            System.out.println("++前：i="+i);
            if (i++ > m) {
                m = p + i;
            }
            System.out.println("++后：i="+i);
        }
        System.out.println("i equals " + i);
    }

}
