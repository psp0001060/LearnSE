package com.d0305.race;

public class Racing {
    int topSpeed =100;

    void upgradeEngine(){
        RepairFactory factory = new RepairFactory();
        factory.upgradeEngine(this);
    }

    public static void main(String[] args) {
        Racing racing = new Racing();
        System.out.println("升级前速度="+racing.topSpeed);
        racing.upgradeEngine();
        System.out.println("升级后速度="+racing.topSpeed);
    }
}
