package com.d0311;

public interface IWeapon {
    //接口中只存在公开静态常量
    public static final int ATTACK_SPEED_FAST=200;

    //接口中，即使删除常量前面的public static final ,系统也是默认提供的
    int ATTACK_SPEED_SLOW=50;

    public abstract void attack(); //只有方法声明，没有方法体

    void defense(); //接口中，即使删除常量前面的public abstract ,系统也是默认提供的

//    public IWeapon(); //接口中没有构造方法

    void repairMechine();
}
