package com.d0311;

public enum Color {
    RED("红色",1),GREEN("绿色",2),YELLO("黄色",3);

    private  String zhname;
    private int index;

    private Color(String zhname, int index){
        this.zhname = zhname;
        this.index= index;
    }

    public String getZhname(){
        return zhname;
    }
}
