package com.d0311;

public class Tank extends Vehiche implements IWeapon, IMaintain {
    @Override
    public void attack() {
        System.out.println("Tank attack");
    }

    @Override
    public void defense() {
        System.out.println("Tank defense");
    }

    public static void main(String[] args) {
        Tank tank = new Tank();
        tank.attack();
        tank.start();
        tank.repairMechine();
    }

    @Override
    public void repairMechine() {
        System.out.println("维修tank引擎");
    }
}
