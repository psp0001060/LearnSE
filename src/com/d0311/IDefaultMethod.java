package com.d0311;

public interface IDefaultMethod {
    default void foo(){
        System.out.println("接口提供的默认方法");
    }
}

class DefultMethodImpl implements IDefaultMethod{
    public static void main(String[] args) {
        DefultMethodImpl defultMethodimpl = new DefultMethodImpl();
        defultMethodimpl.foo();
    }

}
