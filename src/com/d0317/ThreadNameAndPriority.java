package com.d0317;

public class ThreadNameAndPriority {
    public static void main(String[] args) {
        ThreadGroup group = new ThreadGroup("ChinasoftiThreadGroup");
        Thread thread1 = new Thread(group, "th1");
        Thread thread2 = new Thread("th2");

        thread2.setPriority(Thread.MAX_PRIORITY);
        System.out.println(thread1);
        System.out.println(thread2);
    }
}
