package com.d0317;

public class DaemonThreadTest {
    public static void main(String[] args) {
        DaemonThread daemonThread = new DaemonThread();
        daemonThread.setDaemon(true); //守护线程
        daemonThread.start();

    }
}


class DaemonThread extends Thread{
    @Override
    public void run() {
        while (true){
            System.out.println("当前线程执行中");
        }
    }
}