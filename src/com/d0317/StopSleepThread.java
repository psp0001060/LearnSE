package com.d0317;

public class StopSleepThread {
    public static void main(String[] args) {
        SleepThread sleepThread = new SleepThread();
        System.out.println("线程启动");
        sleepThread.start();
        sleepThread.interrupt();
    }

}

class SleepThread extends Thread {
    boolean flag = true;

    @Override
    public void run() {
        while (flag) {
            try {
                System.out.println("线程正在运行");
                sleep(1000 * 60); //休眠1分钟
            } catch (InterruptedException e) {
//                e.printStackTrace();
                System.out.println("线程中断");
                flag = false;
            }
        }
    }
}