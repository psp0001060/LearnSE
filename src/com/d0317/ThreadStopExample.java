package com.d0317;

public class ThreadStopExample extends Thread{
    private boolean isRunning = true;
    @Override
    public void run() {
        while (isRunning) {
            System.out.println("当前线程正在运行");
        }
    }

    public void stopThread(){
        isRunning = false;
    }

    public static void main(String[] args) {
        ThreadStopExample threadStopExample = new ThreadStopExample();
        threadStopExample.start();
        try {
            sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        threadStopExample.stopThread();
    }
}
