package com.d0317;

import javafx.scene.paint.Stop;

import java.util.Scanner;

public class StopIOWaitThread {
    public static void main(String[] args) {
        IOWaitThread stopIOWaitThread = new IOWaitThread();
        System.out.println("线程启动");
        stopIOWaitThread.start();
        stopIOWaitThread.scanner.close(); //关闭底层资源后，线程读取操作引发异常，从而中断
    }
}


class IOWaitThread extends Thread {
    Scanner scanner = new Scanner(System.in);
    private boolean flag = true;

    @Override
    public void run() {
        while (flag) {
            try {
                System.out.println("等待用户输入：");
                scanner.nextLine();
            }catch (Exception ex){
                System.out.println("线程中断");
                flag = false;
            }
        }
    }

    public void stopThread() {
        flag = false;
    }
}