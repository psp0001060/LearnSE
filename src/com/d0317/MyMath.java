package com.d0317;

import java.util.Scanner;

public class MyMath {
    public int div(String x, String y) throws ArithmeticException,NumberFormatException{
        int result = 0;

        int a = Integer.parseInt(x);
        int b = Integer.parseInt(y);

        result = a / b;
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String x = scanner.next();
        String y = scanner.next();
        MyMath myMath = new MyMath();
        try {
            System.out.println(myMath.div(x, y));
        }catch (NumberFormatException e){
            System.out.println("输入格式不正确");
        }catch (ArithmeticException e){
            System.out.println("产生了数学异常，请检查数字是否合法");
        }catch (Exception e){
            System.out.println("发生了异常");
        }
    }
}
