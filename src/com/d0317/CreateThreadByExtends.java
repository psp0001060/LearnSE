package com.d0317;

public class CreateThreadByExtends extends Thread {
    @Override
    public void run() {
        System.out.println("CreateThreadByExtends is running.");
        System.out.println(currentThread().getName());
    }

    public static void main(String[] args) {
        CreateThreadByExtends createThreadByExtends = new CreateThreadByExtends();
        createThreadByExtends.setName("ct1");
        CreateThreadByExtends createThreadByExtends2 = new CreateThreadByExtends();
        createThreadByExtends2.setName("ct2");
//        createThreadByExtends.run();// 错误的调用方式，不会产生并发效果
        createThreadByExtends.start(); //正确
        createThreadByExtends2.start(); //正确
    }
}
