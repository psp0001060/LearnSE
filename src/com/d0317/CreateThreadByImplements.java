package com.d0317;

public class CreateThreadByImplements implements Runnable {
    @Override
    public void run() {
        System.out.println("CreateThreadByImplements is running...");
        System.out.println(Thread.currentThread().getName());
    }

    public static void main(String[] args) {
        Thread threadByImplement = new Thread(new CreateThreadByImplements(),"tbi");
        threadByImplement.start();
    }
}
