package com.d0327;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class WordCount {
    public static void main(String[] args) throws Exception {
        File f = new File("D:\\a.txt");
        BufferedReader br = new BufferedReader(new FileReader(f));

        Map<String, Integer> map = new HashMap<>();
        String str = null;
        while ((str = br.readLine()) != null) {
            String[] words = str.split("[ \n.,]");

            for (String word :
                    words) {
                if (word.equals("")) {
                    continue;
                }
                if (map.get(word) == null) {//第一次存储
                    map.put(word, 1);
                } else {//非第一次存储
                    int oldValue = map.get(word);
                    map.put(word, oldValue + 1);
                }
            }

        }
        map.forEach((k, v) -> System.out.println(k + "\t" + v));
    }
}
