package com.d0316;

public class SecondException {
    public static void main(String[] args) {
        try {
            int x = 100;
            int y = 10;
            System.out.println("x/y=" + x / y);
            System.out.println("world");
            System.exit(0); // finally 不再执行
//            return; //执行finally后，再return
        } catch (NullPointerException ex) {
            System.out.println("发生了空指针异常");
        }catch (Exception e){
            System.out.println("发生了异常");
//            return;
        }finally {//无论程序是否发生异常，均会执行
            System.out.println("finally 执行");
        }

        System.out.println("程序运行结束");
    }
}
