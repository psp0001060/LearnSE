package com.d0316;


public class DataValueException extends Exception {

    public DataValueException() {
    }

    public DataValueException(String message) {
        super(message);
    }
}
