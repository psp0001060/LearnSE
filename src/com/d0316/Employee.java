package com.d0316;

public class Employee {
    private String name;
    private int salary;

    public Employee() {
    }

    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public void setSalary(int salary) throws DataValueException {
        if (salary <=2000){
            throw new DataValueException("薪资不能低于2000");
        }
        this.salary = salary;
    }

    public static void main(String[] args) {
        Employee employee = new Employee();
        try {
            employee.setSalary(2300);
        } catch (DataValueException e) {
            e.printStackTrace();
        }
    }
}
