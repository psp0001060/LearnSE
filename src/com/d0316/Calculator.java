package com.d0316;

import java.io.FileNotFoundException;

public class Calculator {
    public void div(int x, int y) throws Exception {
        if (y == 0) {
            throw new Exception("除数不能为零");
        }
        System.out.println(x/y);
    }

    public void callDiv(){
        try {
            div(19,8);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.callDiv();
    }
}
