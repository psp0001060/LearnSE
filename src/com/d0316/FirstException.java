package com.d0316;

import java.io.File;
import java.io.FileReader;

public class FirstException {
    public static void main(String[] args) {
//        int a = 100/0; //运行时异常

        /*File file = new File("d:/a.txt");
        FileReader fr = new FileReader(file); //非运行时异常
        */

//        Object obj = new Object();
//        String str = (String) obj;

//        String s = "abc";
//        Integer i1 = Integer.parseInt(s); //将字符串转成整数类型
//        System.out.println(i1+5);
        System.out.println(12.3/0);
    }

}
