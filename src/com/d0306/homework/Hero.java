package com.d0306.homework;


public class Hero {
    int level;//级别
    int exp; //经验值

    public Hero(int exp){
        if (exp<0){
            this.exp=0;
            this.level=0;
        }else if (exp>getMaxExp()){
            this.exp = getMaxExp();
            this.level=getMaxLevel();
        }else {
            this.exp=exp;
            int level=0;
            //当前经验值：5000-getExp(3)
            while ((exp=exp-getExp(level+1))>=0){
                level++;
            }
            this.level=level;
        }
    }

    public Hero(){
        this(0);
    }

    int getMaxLevel(){
        return 30;
    }

    int getMaxExp(){
        return getExp(30);
    }

    /**
     * 根据级别算出经验值
     * @param level 级别
     * @return 经验值
     */
    int getExp(int level){
        return 30*(level*level*level+5*level)-80;
    }

    public static void main(String[] args) {
        System.out.println("===============无参构造======================");
        Hero hero = new Hero();
        System.out.println(hero.exp);
        System.out.println(hero.level);

        System.out.println("===============有参构造======================");
        Hero hero2 = new Hero(50000);
        System.out.println(hero2.exp);
        System.out.println(hero2.level);
    }
}
