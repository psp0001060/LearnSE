package com.d0310;

public class HumanStatic {
    public static String population = "灵长目";
    public String name;


    //静态成员方法
    public static void setPopulation(String newPopulation){
//        name=""; static方法中不能直接访问非static的成员变量
        population = newPopulation;
    }

    public void foo(){
        population=""; //实例方法中可以直接访问static成员变量
        setPopulation("");//实例方法中可以直接调用static方法
    }

    public static void main(String[] args) {
//        setPopulation(""); //static方法可以直接调用本类中声明的其他static方法
        //可以通过类直接调用静态成员方法
        HumanStatic.setPopulation("灵长目2");

        //可以通过类直接调用静态成员变量
        System.out.println(HumanStatic.population);
    }
}
