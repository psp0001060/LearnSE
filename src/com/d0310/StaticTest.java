package com.d0310;

public class StaticTest {


    public static void main(String[] args) {
        Cat cat=null;
        for (int i = 0; i < 5; i++) {
             cat = new Cat();
             new Dog();
        }
        System.out.println(cat.counter);
        System.out.println(Dog.counter);

        System.out.println(Cat.VERSION);
    }
}

class Cat{
    public static final String VERSION = "V2.4"; //静态常量

    public int counter=0;
    public Cat(){
        counter++;
    }
}

class Dog{
    public static int counter=0;
    public Dog(){
        counter++;
    }
}
