package com.d0310;

public class FinalVal {
    final int i = 10;

    public void foo() {
//        i = 20; //错误，常量不能再次赋值
    }
}
