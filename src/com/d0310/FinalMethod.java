package com.d0310;

public class FinalMethod {
    public final void foo(){

    }
}

class FinalMethodSub extends FinalMethod{

    //父类的foo方法是final修饰的，所以不能够被子类覆盖
/*    public void foo(){

    }*/
}