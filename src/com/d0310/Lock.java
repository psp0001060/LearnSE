package com.d0310;

//基类

/**
 * 1.含有抽象方法的类一定是抽象类
 * 2.抽象类不一定含有抽象方法
 */
public abstract class Lock {
    public abstract void open(); //抽象方法，只有方法签名，没有方法体

    public  void close(){

    }
}

class FingerprintLock extends Lock{

    @Override
    public void open() {
        System.out.println("指纹锁的open方法");
    }


}

//实现一个派生类FaceLock
class FaceLock extends Lock{

    @Override
    public void open() {
        System.out.println("人脸锁的open方法");
    }
}
class TestLock{
    public static void main(String[] args) {
//        Lock lock = new Lock(); //抽象类不能实例化
        FingerprintLock fingerprintLock = new FingerprintLock();
        fingerprintLock.open();
    }
}
